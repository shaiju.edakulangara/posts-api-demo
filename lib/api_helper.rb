module ApiHelper
  def validate_auth_token
    get_current_user
    render :status => 401, :json => {errors: [t('api.v1.token.invalid_token')]} if @user.nil?
  end

  def get_auth_token
    if params[:auth_token].blank?
      logger.debug "Auth Token: #{request.headers[Constants::AUTH_TOKEN]}"
      params[:auth_token] = request.headers[Constants::AUTH_TOKEN]
    end
  end

  def get_current_user
    @user = User.find_by_authentication_token(params[:auth_token]) unless params[:auth_token].blank?
  end
end
