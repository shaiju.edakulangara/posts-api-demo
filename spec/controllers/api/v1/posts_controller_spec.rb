require 'rails_helper'

RSpec.describe Api::V1::PostsController, type: :controller do

  before(:each) do
    @user = FactoryBot.create(:user, authentication_token: 'asdasd')

    @post = FactoryBot.create(:post, user_id: @user.id)
  end

  describe "List Posts" do
    it "fetch the posts" do
      request.headers[Constants::AUTH_TOKEN] = @user.authentication_token
      get :index,  :format => :json
      response.code.should eq("200")
      json_data = JSON.parse(response.body)
      expect(json_data["posts"][0]['title']).to eql(@post.title)
      expect(json_data["posts"][0]['url']).to eql(@post.url)
      expect(json_data["posts"][0]['user_id']).to eql(@post.user_id)
    end
  end

  describe "Create Post" do
    before(:each) do
      @user = FactoryBot.create(:user, email: 'a@example.com', authentication_token: 'adasd')
    end

    it "create the post" do
      request.headers[Constants::AUTH_TOKEN] = @user.authentication_token
      post :create, post: { title: 'test post', url: 'www.example.com'}
      response.code.should eq("200")

      post = Post.last
      expect(post.title).to eql(@project)
      json_data = JSON.parse(response.body)
      expect(json_data['message']).to eql('Successfully created the Post')
    end

    it "return bad request message for invalid post" do
      request.headers[Constants::AUTH_TOKEN] = @user.authentication_token
      post :create, post: {}
      response.code.should eq("400")
    end
  end
end
