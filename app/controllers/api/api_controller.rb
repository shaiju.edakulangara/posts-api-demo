class Api::ApiController < ActionController::Base
  include ApiHelper
  prepend_before_action :get_auth_token
  before_action :validate_auth_token
  skip_before_action :verify_authenticity_token, if: Proc.new { |c| c.request.format == 'application/json' }
  respond_to :json
end
